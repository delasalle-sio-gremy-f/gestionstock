import { Injectable } from '@angular/core';
import {Produit} from './produits';

@Injectable({
  providedIn: 'root'
})
export class ListProduitService  {

 _PRODUIT: Array<Produit> = []

  constructor() { }

  list(){
    this._PRODUIT = [
      new Produit("Tarte aux pommes", "tartapain", "tartapin@gmail.com",['farine', 'pommes'], "une tarte", 1, "A conserver a temperature ambiante", 9),
      new Produit( "Tarte aux poires", "tartapain", "tartapin@gmail.com",['farine', 'poires'], "une tarte", 1, "A conserver a temperature ambiante", 7),
      new Produit( "Tarte aux mirtilles", "tartapain", "tartapin@gmail.com",['farine', 'mirtilles'], "une tarte", 1, "A conserver a temperature ambiante", 8),
      new Produit( "Tarte aux gingembres", "tartapain", "tartapin@gmail.com",['farine', 'gingembres'], "une tarte", 3, "A conserver a temperature ambiante", 5),
      new Produit( "Tarte aux patates", "tartapain", "tartapin@gmail.com",['farine', 'patates'], "une tarte", 2, "A conserver a temperature ambiante", 12)
    ];
    return this._PRODUIT
  }

  SupprimerListe(index){
    this._PRODUIT.splice(index, 1)
  }

}
