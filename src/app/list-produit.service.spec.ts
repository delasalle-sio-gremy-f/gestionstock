import { TestBed } from '@angular/core/testing';

import { ListProduitService } from './list-produit.service';

describe('ListProduitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListProduitService = TestBed.get(ListProduitService);
    expect(service).toBeTruthy();
  });
});
