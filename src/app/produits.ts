export class Produit {
    nom: string;
    fournisseur: string;
    emailFournisseur: string;
    ingredient: Array<string>;
    description: string;
    age: number;
    conditionConservation: string;
    prix: number;

    constructor(nom, fournisseur, emailFournisseur, ingredient, description, age, conditionConservation, prix){
        this.nom = nom;
        this.fournisseur = fournisseur;
        this.emailFournisseur = emailFournisseur;
        this.ingredient = ingredient;
        this.description = description;
        this.age = age;
        this.conditionConservation = conditionConservation;
        this.prix = prix;
    }

}