import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from'@angular/forms'
import { ListProduitService } from './list-produit.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ListProduitService]
})

export class AppComponent implements OnInit {
  formHidden = true
  List = []

  constructor(private ProduitsList: ListProduitService) {}

  ngOnInit() {
    this.List = this.ProduitsList.list()
  }

   SupprimerListe(index){
    this.List.splice(index, 1)
  }

  AddForm(){
    this.formHidden = false
  }




  
  userForm: FormGroup;
  nomCtrl: FormControl;
  fournisseurCtrl: FormControl;
  ageCtrl: FormControl;
  descriptionCtrl: FormControl;

  form(fb: FormBuilder) {
    this.nomCtrl = fb.control('', Validators.required);
    this.fournisseurCtrl = fb.control('', Validators.required);
    this.ageCtrl = fb.control('', Validators.required);
    this.descriptionCtrl = fb.control('', Validators.required);
    this.userForm = fb.group({
      nom: this.nomCtrl,
      fournisseur: this.fournisseurCtrl,
      age: this.ageCtrl,
      description: this.descriptionCtrl
    });
  }
  
  register() {
    console.log(this.userForm.value);
  }

}
